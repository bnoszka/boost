### Instalacja

```
git clone https://gitlab.com/bnoszka/boost.git boost
```

```
cd boost
```

W pliku `.env` zmienić dostęp do bazy:

`DATABASE_URL=mysql://root@127.0.0.1:3306/boosty?serverVersion=5.7`

Tylko dla zapisywanie galerii poprzez udostępnione API - nieskończone
`DOMAIN=https://127.0.0.1:8008/`

```
composer install
```

```
bin/console doctrine:database:create
```

```
bin/console doctrine:schema:update --force
```

Pobieranie galerii:

```
bin/console app:crawl
```

Uruchomienie serwera www.

```
sf serve -d
```

Więcej informacji: `https://symfony.com/doc/current/setup/symfony_server.html`

Output
```
[OK] Web server listening on https://127.0.0.1:8009 (PHP FPM 7.3.10)
```


Galerię powinny się wyświetlić na stronie głownej (zmienić port lub domeną):

`https://127.0.0.1:8009/`



### API

Dokumentacja API dostępne pod linkiem `/api/docs`.

Przykład dla `/api/galleries` `GET`

```
{
  "@context": "\/api\/contexts\/Gallery",
  "@id": "\/api\/galleries",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "\/api\/galleries\/1",
      "@type": "Gallery",
      "id": 1,
      "title": "June 2018 Fawn and Mom Typical Fawn! (18 images)",
      "date": "2018-06-01T00:00:00+02:00",
      "photos": [
        {
          "id": 1,
          "url": "http:\/\/www.watchthedeer.com\/..\/looping_images\/June_2018_Fawn\/LV20180625172930000N.jpg"
        },
        {
          "id": 2,
          "url": "http:\/\/www.watchthedeer.com\/..\/looping_images\/June_2018_Fawn\/LV20180625172940000N.jpg"
        },
        ...
      ]
    },
    {
      "@id": "\/api\/galleries\/2",
      "@type": "Gallery",
      "id": 2,
      "title": "Young velvet buck June 2 2018 (135 images)",
      "date": "2018-06-02T00:00:00+02:00",
      "photos": [
        {
          "id": 19,
          "url": "http:\/\/www.watchthedeer.com\/looping_images\/Young%20velvet%20buck%20June%202%202018\/LV20180602124724000N.jpg"
        },
        ...
      ]
    },
    {
      "@id": "\/api\/galleries\/3",
      "@type": "Gallery",
      "id": 3,
      "title": "South Georgia Nice- typical Buck (55 images)",
      "date": "2017-11-01T00:00:00+01:00",
      "photos": [
        {
          "id": 154,
          "url": "http:\/\/www.watchthedeer.com\/looping_images\/Nov%202017%20Ga%20Kudzu%20Buck\/LV20171121235056000N.jpg"
        },
        ...
      ]
    },
    ...
  ],
  "hydra:totalItems": 20
}
```

### Screen

![](cli_output.png)

![](galleries_screen.png)

![](gallery_screen.png)



### TODO:

1. Refaktoryzacja klasy `App\Crawler\Downloader` poprzez rozdzieleczenie "strumienia" dla kolekcji oraz dla pozycji do różnych klas.
2. DONE. ~~Zmiana komendy do zapisywania galerii nie poprzez klasę `GalleryManager`, ale poprzez udostępnione API. Zalążek architektury mikroserwisowej - wtedy katalog src/Crawler należy przenieść do osobnego projektu.~~
3. Zmienić architekturę CRUD'ową na coś nowocześniejszego:
   1. Zaimplementować CQRS poprzez komponent symfony/messanger
   2. Stworzyć DTO (input, output) dla wszystkich endpointów wraz z klasami Query oraz Handler
   3. Stworzyć klasy Factory
4. Dostować Crawlera do pobierania galerii video, poprzez zaimplementowanie Crawlera z obsługą JS'a (Symfony Panther?)    
5. Do zastanowienia się:
   1. Stworzyć jeden endpoint dla API oraz WEB. Rozpoznanie jakiego typu kontentu mamy zwrócić (JSON albo HTML) powinno się odbywać za pomocą nagłowka w REQUEST - Accept-Content.  













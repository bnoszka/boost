<?php

declare(strict_types=1);

namespace App\Crawler\Task;

class DateCreate
{
    private $fields;

    public function __construct(array $fields = [])
    {
        $this->fields = $fields;
    }

    public function __invoke($item)
    {
        foreach ($this->fields as $field)
        {
            $content = $item[$field];
            $content = $content === '' ? NULL : date_create($content);
            $content = $content === false ? NULL : $content;
            $item[$field] = $content;
        }
        return $item;
    }
}

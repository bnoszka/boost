<?php

declare(strict_types=1);

namespace App\Crawler\Task;

class FixStripSpaces
{
    private $fields;

    public function __construct(array $fields = [])
    {
        $this->fields = $fields;
    }

    public function __invoke($item)
    {
        foreach ($this->fields as $field)
        {
            $content = $item[$field];
            $content = trim(preg_replace('/\s+/', ' ', $content));
            $item[$field] = $content;
        }
        return $item;
    }
}

<?php

declare(strict_types=1);

namespace App\Crawler\Task\WatchTheDeer;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Panther\Client;

class CollectionAppendGalleries
{
    private $definition;

    /** @var Client  */
    private $httpClient;
    private $crawler;
    private $limit;

    public function __construct($definition, $limit = null)
    {
        $this->definition = $definition;
        $this->httpClient = HttpClient::create();
        $this->crawler = new Crawler();
        $this->limit = $limit;
    }

    public function __invoke($collection)
    {
        $response = $this->httpClient->request('GET', $this->definition['url']);

        $this->crawler->addHtmlContent( $response->getContent() );

        $count = 0;
        $this->crawler->filterXPath('//div[@id="content"]//li//a')->each(function($node) use (&$collection, &$count) {

            if( $this->limit > $count )
            {
                $isVideo =
                    substr( $node->attr('href'), 0, 10 ) === "javascript"
                    ||
                    strpos($node->text(), 'video') !== false
                    ;

                if( ! $isVideo )
                {
                    $item = [
                        'title' => $node->text(),
                        'helpers' => [
                            'fullUrl' => 'http://www.watchthedeer.com/'.$node->attr('href'),
                            'isGallery' => !$isVideo
                        ],
                    ];
                    $collection[] = $item;
                    $count++;
                }
            }

        });

        return $collection;

    }
}

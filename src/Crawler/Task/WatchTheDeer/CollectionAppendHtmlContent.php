<?php

declare(strict_types=1);

namespace App\Crawler\Task\WatchTheDeer;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class CollectionAppendHtmlContent
{
    private $definition;
    private $httpClient;

    public function __construct($definition)
    {
        $this->definition = $definition;
        $this->httpClient = HttpClient::create();
    }

    public function __invoke($collection)
    {
        foreach($collection as $key => $item)
        {
            $response = $this->httpClient->request('GET', $item['helpers']['fullUrl']);
            $collection[$key]['helpers']['htmlContent'] = $response->getContent();
        }
        return $collection;
    }
}

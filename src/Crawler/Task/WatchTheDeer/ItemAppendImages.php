<?php

declare(strict_types=1);

namespace App\Crawler\Task\WatchTheDeer;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class ItemAppendImages
{
    private $definition;
    private $httpClient;
    private $crawler;

    public function __construct($definition)
    {
        $this->definition = $definition;
        $this->httpClient = HttpClient::create();
        $this->crawler = new Crawler();
    }

    public function __invoke($item)
    {
        $item['images'] = [];

        $this->crawler->addHtmlContent( $item['helpers']['htmlContent'] );

        $myImage = [];

        $this->crawler->filterXPath('//script[@language="javascript"]')->each(function($node, $index) use (&$myImage) {
            $text = $node->text();
            $text = str_replace('var myImage = new Array();', '', $text );
            $text = str_replace('myImage', '$myImage', $text );
            eval($text);
        });

        $images = [];
        foreach($myImage as $tempImage)
        {
            $images[] = rtrim($item['helpers']['fullUrl'], 'viewer.aspx') . $tempImage;
        }
        $item['images'] = $images;
        return $item;
    }
}

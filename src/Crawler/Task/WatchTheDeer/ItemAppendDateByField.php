<?php

declare(strict_types=1);

namespace App\Crawler\Task\WatchTheDeer;

use Symfony\Component\PropertyAccess\PropertyAccess;

class ItemAppendDateByField
{
    private $definition;
    private $field;
    private $propertyAccessor;

    public function __construct($definition, $field)
    {
        $this->definition = $definition;
        $this->field = $field;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    public function __invoke($item)
    {
        $content = $this->propertyAccessor->getValue($item, $this->field);

        $matches = [];

        if( array_key_exists('date', $item) && $item['date'] !== '' )
        {
            return $item;
        }

        $date = '';

        if( null !== $content)
        {
            preg_match('/([A-Za-z]{3,9}\s+[0-9]{4})|([A-Za-z]{3,9}\s+[0-9]{1,2}\s+[0-9]{4})/', $content,$matches);

            if( ! empty ($matches))
            {
                $date = $matches[0];

            }
        }

        $item['date'] = $date;

        return $item;
    }
}

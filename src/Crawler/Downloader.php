<?php

namespace App\Crawler;

use App\Crawler\Task\DateCreate;
use App\Crawler\Task\FixStripSpaces;
use App\Crawler\Task\WatchTheDeer\CollectionAppendGalleries;
use App\Crawler\Task\WatchTheDeer\CollectionAppendHtmlContent;
use App\Crawler\Task\WatchTheDeer\ItemAppendDateByField;
use App\Crawler\Task\WatchTheDeer\ItemAppendImages;
use League\Pipeline\Pipeline;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Downloader
{
    public const WTD_DEFINITION = [
        'service' => 'wtd',
        'url' => 'http://www.watchthedeer.com/photos',
    ];

    public const ACTIVE_DEFINITIONS = [self::WTD_DEFINITION];

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function parse()
    {
        $collection = [];

        foreach(self::ACTIVE_DEFINITIONS as $definition)
        {
            switch ($definition['service']) {
                case 'wtd':

                    $definitionCollections = [];

                    $pipeline = (new Pipeline())
                        ->pipe(new CollectionAppendGalleries($definition, 20))
                        ->pipe(new CollectionAppendHtmlContent($definition))
                    ;
                    $definitionCollections = $pipeline->process( $definitionCollections );

                    foreach($definitionCollections as $item) {

                        $pipelineItem = (new Pipeline())
                            ->pipe(new ItemAppendImages($definition))
                            ->pipe(new ItemAppendDateByField($definition, '[title]'))
                            ->pipe(new ItemAppendDateByField($definition, '[helpers][htmlContent]'))
                            ->pipe(new FixStripSpaces(['title']))
                            ->pipe(new DateCreate(['date']))
                        ;
                        $collection[] = $pipelineItem->process( $item );

                    }
                    break;
            }
        }
        return $collection;
    }
}

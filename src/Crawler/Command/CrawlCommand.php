<?php

namespace App\Crawler\Command;

use App\Crawler\Downloader;
use App\Gallery\Entity\Gallery;
use App\Gallery\Entity\Photo;
use App\Gallery\Manager\GalleryManager;
use GuzzleHttp\RequestOptions;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CrawlCommand extends Command
{
    protected static $defaultName = 'app:crawl';
    protected $downloader;
    protected $galleryManager;
    protected $client;

    public function __construct(Downloader $downloader, GalleryManager $galleryManager, ContainerInterface $container, string $name = null)
    {
        parent::__construct($name);
        $this->downloader = $downloader;
        $this->galleryManager = $galleryManager;

        $this->client = new Client([
            'base_uri' => $container->getParameter('domain'),
            'verify' => false
        ]);

    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $collection = $this->downloader->parse();

        $table = new Table($output);
        $table->setHeaders(['Title', 'Photos', 'Date', 'Error']);

        $rows = [];

        foreach ($collection as $item) {

            $row = [
                $item['title'],
                count( $item['images'] ),
                NULL == $item['date'] ? '-' : $item['date']->format('Y-m-d'),
            ];

            $photos = array_map(function($image) {
                return ['url' => $image];
            }, $item['images']);

            $res = $this->client->post('/api/galleries', [
                RequestOptions::JSON => [
                    'title' => $item['title'],
                    'date' => $item['date']->format('Y-m-d'),
                    'photos' => $photos
                ],
                RequestOptions::HEADERS => [
                    'Content-Type' => 'application/json'
                ]
            ]);

            $res->getStatusCode() !== 201 ? $row[] = 'YES' : $row[] = '';

            $rows[] = $row;
        }

        $table
            ->setRows( $rows )
            ->render()
        ;
    }
}

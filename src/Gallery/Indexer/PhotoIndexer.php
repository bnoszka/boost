<?php

namespace App\Gallery\Indexer;

use App\Gallery\Manager\PhotoManager;

class PhotoIndexer extends AbstractIndexer
{
    /**
     * @required
     */
    public function init(PhotoManager $photoManager)
    {
        $this->manager = $photoManager;
    }
}

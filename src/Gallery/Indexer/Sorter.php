<?php

namespace App\Gallery\Indexer;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class Sorter
{
    protected $currentProperty;
    protected $currentOrder;
    protected $router;
    protected $request;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function process(Request $request, $queryBuilder, $class)
    {
        $this->request = $request;

        if ( ! $queryBuilder instanceof QueryBuilder)
        {
            throw new \InvalidArgumentException('Default sorter supports only "Doctrine\\ORM\\QueryBuilder" as sortable argument.');
        }

        if (null === $this->currentProperty = $request->query->get('sort', null))
        {
            return;
        }
        
        $this->currentOrder = $request->query->get('order', 'ASC');

        if (!in_array($this->currentOrder, array('ASC', 'DESC'))) {

            return;
        }

        if(strpos($this->currentProperty, '.'))
        {
            $queryBuilder->orderBy($this->currentProperty, $this->currentOrder);
        }
        else
        {
            $reflectionClass = new \ReflectionClass($class);

            if ( ! in_array( $this->currentProperty, array_keys( $reflectionClass->getDefaultProperties() ) ) )
            {
                $queryBuilder->orderBy($this->currentProperty, $this->currentOrder);
                return;
            }

            $queryBuilder->orderBy($queryBuilder->getRootAliases()[0].'.' . $this->currentProperty, $this->currentOrder);
        }
    }

    public function getPath($property)
    {
        $params = $this->request->get('_route_params');
        $params = array_merge($params, $this->request->query->all() );
        $params = array_merge($params, ['sort' => $property, 'order' => ($this->currentOrder == 'ASC' ? 'DESC' : 'ASC') ] );
        return $this->router->generate($this->request->get('_route'), $params);
    }


    public function getIconClass($property)
    {
        if( $this->currentProperty == $property)
        {
            if($this->currentOrder == 'ASC')
            {
                return '↓';
            }
            return '↑';
        }
        return '↕';
    }

}

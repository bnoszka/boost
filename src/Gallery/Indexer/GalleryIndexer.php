<?php

namespace App\Gallery\Indexer;

use App\Gallery\Manager\GalleryManager;

class GalleryIndexer extends AbstractIndexer
{
    /**
     * @required
     */
    public function init(GalleryManager $galleryManager)
    {
        $this->manager = $galleryManager;
    }
}

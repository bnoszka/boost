<?php

namespace App\Gallery\Indexer;

use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

abstract class AbstractIndexer
{
    const DEFAULT_PER_PAGE = 5;
    const DEFAULT_PAGE = 1;


    protected $manager;
    protected $sorter;
    protected $paginator;
    /** @var Request */
    protected $request;
    protected $router;
    protected $perPage;
    protected $page;

    public function __construct(Sorter $sorter, RouterInterface $router)
    {
        $this->sorter = $sorter;
        $this->router = $router;
    }

    public function process(Request $request, ?QueryBuilder $queryBuilder = null): void
    {
        if( ! $queryBuilder)
        {
            $queryBuilder = $this->manager->getPluralQueryBuilder();
        }

        $this->request = $request;

        $this->perPage = $request->query->has( 'perPage' ) ? $request->query->get( 'perPage' ) : self::DEFAULT_PER_PAGE;
        $this->page = $request->query->has( 'page' ) ? $request->query->get( 'page' ) : self::DEFAULT_PAGE;

        $this->sorter->process($request, $queryBuilder, $this->manager->getClass());

        $this->paginator = new Pagerfanta(new DoctrineORMAdapter( $queryBuilder->getQuery() ));
        $this->paginator->setMaxPerPage( $this->perPage );
        $this->paginator->setCurrentPage( $this->page );
    }

    public function getResults()
    {
        return $this->paginator->getCurrentPageResults();
    }

    public function getPaginator()
    {
        return $this->paginator;
    }

    public function getSorter()
    {
        return $this->sorter;
    }

    public function firstPageIndex()
    {
        return $this->perPage * ($this->page - 1);
    }

    public function getPathPerPage($perPage)
    {
        $params = array_merge($this->request->get('_route_params'), $this->request->query->all());
        $params['perPage'] = $perPage;
        $params['page'] = 1;
        return $this->router->generate($this->request->get('_route'), $params);
    }
}

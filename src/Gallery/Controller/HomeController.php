<?php

namespace App\Gallery\Controller;

use App\Gallery\Indexer\GalleryIndexer;
use App\Gallery\Indexer\PhotoIndexer;
use App\Gallery\Manager\GalleryManager;
use App\Gallery\Manager\PhotoManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    protected $photoManager;
    protected $galleryManager;

    public function __construct(GalleryManager $galleryManager, PhotoManager $photoManager)
    {
        $this->photoManager = $photoManager;
        $this->galleryManager = $galleryManager;
    }

    /**
     * @Route("/", name="gallery_index", methods={"GET"})
     */
    public function list(Request $request, GalleryIndexer $indexer)
    {
        $indexer->process($request);

        return $this->render('gallery/list.html.twig', [
            'indexer' => $indexer
        ]);
    }

    /**
     * @Route("/show/{id}", name="gallery_show", methods={"GET"})
     */
    public function show(Request $request, PhotoIndexer $indexer, $id)
    {
        $gallery = $this->galleryManager->find($id);

        $indexer->process($request, $this->photoManager->getPhotosByGalleryQueryBuilder($gallery));

        return $this->render('gallery/show.html.twig', [
            'indexer' => $indexer
        ]);
    }


}

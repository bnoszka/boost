<?php

namespace App\Gallery\Manager;

use App\Gallery\Entity\Gallery;
use App\Gallery\Entity\Photo;

class PhotoManager extends AbstractManager
{
    protected $class = Photo::class;

    public function getPhotosByGalleryQueryBuilder(Gallery $gallery)
    {
        return $this->repository
            ->createQueryBuilder('e')
            ->where('e.gallery = :gallery')
            ->setParameter('gallery', $gallery)
            ;
    }
}

<?php

namespace App\Gallery\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class AbstractManager
{
    /** @var EntityManager */
    protected $em;

    /** @var EntityRepository */
    protected $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repository = $em->getRepository( $this->class );
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function persist($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

    public function findAll()
    {
        return $this->repository->findAll();
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function getPluralQueryBuilder()
    {
        return $this->repository->createQueryBuilder('e');
    }
}

<?php

namespace App\Gallery\Manager;

use App\Gallery\Entity\Gallery;

class GalleryManager extends AbstractManager
{
    protected $class = Gallery::class;

    public function findPhotosByGallery()
    {
        return $this->repository;
    }
}

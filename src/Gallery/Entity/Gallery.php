<?php

namespace App\Gallery\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"GALLERY"}}
 *          },
 *          "post"={
 *              "normalization_context"={
 *                  "groups"={"GALLERY"},
 *              },
 *              "denormalization_context"={
 *                  "groups"={"GALLERY"},
 *              }
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={
 *                  "groups"={"GALLERY"},
 *              }
 *          },
 *          "put" = {
 *              "normalization_context"={
 *                  "groups"={"GALLERY"},
 *              },
 *              "denormalization_context"={
 *                  "groups"={"GALLERY"},
 *              }
 *          },
 *          "delete"
 *     },
 * )
 *
 * @ORM\Table(name="galleries")
 * @ORM\Entity()
 */
class Gallery
{
    /**
     * @Groups({"GALLERY"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Groups({"GALLERY"})
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=120, min=2)
     * @ORM\Column(name="title", type="string", length=128)
     */
    protected $title;

    /**
     * @Groups({"GALLERY"})
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    protected $date;

    /**
     * @var Photo[]
     * @Assert\Valid()
     * @Groups({"GALLERY"})
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="gallery", cascade={"persist", "merge"})
     */
    protected $photos;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function getPhotos()
    {
        return $this->photos;
    }

    public function setPhotos($photos)
    {
        foreach($photos as $photo)
        {
            $photo->setGallery($this);
        }
        $this->photos = $photos;
    }

    public function addPhoto(Photo $photo)
    {
        $photo->setGallery($this);
        $this->photos->add($photo);
    }
}
